const express = require('express');
const crypto = require('crypto');
const app = express();

app.get('/', (req, res) => {

    res.json({
        secret_key: crypto.randomBytes(64).toString('hex')
    });
});

app.listen(5000, () => console.log(`server start port: 5000`))
